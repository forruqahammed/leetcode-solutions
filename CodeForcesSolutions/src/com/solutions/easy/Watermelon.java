package com.solutions.easy;

import java.util.Scanner;

public class Watermelon {
    public static void main(String[] args) {
        Watermelon watermelon = new Watermelon();
        Scanner input = new Scanner(System.in);
        int w = input.nextInt();
        System.out.println(watermelon.equalityCheck(w));


        Watermelon watermelon1 = new Watermelon();
        System.out.println(watermelon1);
    }
    String equalityCheck(int w){
        double eachPart = (double) w /2;
        if(eachPart % 2 == 0){
            return "YES";
        }
        return "NO";
    }
}
