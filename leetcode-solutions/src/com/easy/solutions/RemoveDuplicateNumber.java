package com.easy.solutions;

public class RemoveDuplicateNumber {

    public static void main(String[] args) {
        RemoveDuplicateNumber obj = new RemoveDuplicateNumber();
        int[] num = {1, 1, 2};
        System.out.println(obj.removeDuplicates(num));
    }


    // Beats 98.22% users
    public int removeDuplicates(int[] nums) {
        int count = 0;

        for (int i = 0; i < nums.length; i++) {
            if (i < nums.length - 1 && nums[i] == nums[i + 1]) {
                continue;
            }
            nums[count] = nums[i];
            count++;
        }
        return count;
    }
}
