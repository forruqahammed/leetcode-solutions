package com.easy.solutions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ThreeSome {

    public static void main(String[] args) {
        ThreeSome obj = new ThreeSome();
        int[] nums = {-1,0,1,2,-1,-4};
        System.out.println(obj.threeSum1(nums));
    }

    public List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> sum = new ArrayList<>();

        Arrays.sort(nums); // Sort the input array

        for (int i = 0; i < nums.length - 2; i++) {
            if (i == 0 || (i > 0 && nums[i] != nums[i - 1])) { // Avoid duplicate triplets
                int left = i + 1, right = nums.length - 1, target = -nums[i];

                while (left < right) {
                    int sumTwo = nums[left] + nums[right];

                    if (sumTwo == target) {
                        sum.add(Arrays.asList(nums[i], nums[left], nums[right]));

                        while (left < right && nums[left] == nums[left + 1]) left++; // Skip duplicates
                        while (left < right && nums[right] == nums[right - 1]) right--; // Skip duplicates

                        left++;
                        right--;
                    } else if (sumTwo < target) {
                        left++;
                    } else {
                        right--;
                    }
                }
            }
        }

        return sum;
    }

    public List<List<Integer>> threeSum1(int[] nums) {
        List<List<Integer>> sum = new ArrayList<>();
        Arrays.sort(nums);
        int i = 0;
        while (i < nums.length - 2) {
            int j = i + 1;
            while (j < nums.length - 1) {
                if (nums[i] + nums[j] + nums[j + 1] == 0) {
                    List<Integer> data = Arrays.asList(nums[i], nums[j], nums[j + 1]);
                    System.out.println(data);
                    System.out.println(sum.contains(data));
                    if (!sum.contains(data)) {
                        sum.add(data);
                    }
                }

                j++;
            }
            i++;
        }


        return sum;
    }
}