package com.easy.solutions;
public class RomanToInteger {
    public static void main(String[] args) {
        String s = "MCMXCIV";
        RomanToInteger obj = new RomanToInteger();
       int answer = obj.romanToInt(s);System.out.println(answer);
    }

    //Beats 93.39% Users
    public int romanToInt(String s) {
        int sum = 0;

        for(int i = 0; i < s.length(); i++){
            char data = s.charAt(i);
            switch(data){
                case 'I' : {
                    if(i < s.length() -1) {
                        if(s.charAt(i+1) ==  'V'){
                            sum += 4;
                            i++;
                            break;
                        } else if(s.charAt(i+1) == 'X'){
                            sum += 9;
                            i++;
                            break;
                        } else {
                            sum += 1;
                            break;
                        }
                    } else {
                        sum += 1;
                        break;
                    }
                }
                case 'V' : {
                    sum += 5;
                    break;
                }
                case 'X' : {
                    if(i < s.length() -1) {
                        if(s.charAt(i+1) == 'L'){
                            sum += 40;
                            i++;
                            break;
                        } else if(s.charAt(i+1) == 'C'){
                            sum += 90;
                            i++;
                            break;
                        } else {
                            sum += 10;
                            break;
                        }
                    } else {
                        sum += 10;
                        break;
                    }
                }
                case 'L' : {
                    sum += 50;
                    break;
                }
                case 'C' : {
                    if(i < s.length() -1) {
                        if(s.charAt(i+1)== 'D'){
                            sum += 400;
                            i++;
                            break;
                        } else if(s.charAt(i+1) == 'M'){
                            sum += 900;
                            i++;
                            break;
                        } else {
                            sum += 100;
                            break;
                        }
                    } else {
                        sum += 100;
                        break;
                    }
                }
                case 'D' : {
                    sum += 500;
                    break;
                }
                case 'M' : {
                    sum += 1000;
                    break;
                }
            }

        }
        return sum;

    }
}
