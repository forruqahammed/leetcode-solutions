package com.easy.solutions;

public class IndexOfFirstOccurence {
    public static void main(String[] args) {
       String haystack = "sadbutsad";
        String needle = "sad";
        IndexOfFirstOccurence obj = new IndexOfFirstOccurence();
        System.out.println(obj.strStr(haystack, needle));
    }

    //Beats 100% Users
    public int strStr(String haystack, String needle) {
        int result = haystack.indexOf(needle);
        return result;
    }
}
