package com.easy.solutions;

public class JumpGame2 {

    public static void main(String[] args) {
        JumpGame2 obj = new JumpGame2();
        int[] nums = {1,2};
        System.out.println(obj.jump(nums));
    }


    public int jump(int[] nums) {
        if(nums.length == 1) {
            return 0;
        }

        int jump = 0;
        int start = nums[0];

        for (int i = 1; i < nums.length; i++) {
            if(nums[i] >= start){
                jump++;
            }else if(nums[i] == nums.length - 1) {
                jump++;
            }
        }
        return jump;

    }
}
