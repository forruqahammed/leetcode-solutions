package com.easy.solutions;

public class LengthOfWord {

    public static void main(String[] args) {
        String s = "   fly me   to   the moon  ";
        LengthOfWord obj = new LengthOfWord();
        System.out.println(obj.lengthOfLastWord(s));
    }


    //Beats 100% Users
    public int lengthOfLastWord(String s) {
        int count = 0;
        boolean isStart = false;

        for (int i = s.length() - 1; i >= 0; i--) {
            if (s.charAt(i) != ' ') {
                isStart = true;
                count++;
            }

            if (isStart && s.charAt(i) == ' ') {
                return count;
            }

        }
        return count;
    }
}
