package com.easy.solutions;

public class ValidPalindrome {

    public static void main(String[] args) {

        ValidPalindrome obj = new ValidPalindrome();
        System.out.println(obj.isPalindrome1("A man, a plan, a canal: Panama"));
        System.out.println(obj.isPalindrome("race a car"));

    }


    public boolean isPalindrome(String s) {

        StringBuffer f = new StringBuffer();

        for(char c : s.toCharArray()){
            if((c >= '0' && c <= '9') || (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') ){
                f.append(c);
            }
        }

        if(f.toString().equalsIgnoreCase(f.reverse().toString())){
            return true;
        }
        return false;

    }



    //Beets 10 % only
    public boolean isPalindrome1(String s) {
        String f = "";
        String k = "";

        for(int i = s.length()- 1; i>= 0; i--){
            char c = s.charAt(i);
            if((c >= '0' && c <= '9') || (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') ){
                f += c;
                k = c + k;
            }
        }

        if(k.equalsIgnoreCase(f)){
            return true;
        }
        return false;

    }
}
