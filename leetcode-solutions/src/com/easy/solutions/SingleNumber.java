package com.easy.solutions;

import java.util.Arrays;

public class SingleNumber {

    public static void main(String[] args) {
        SingleNumber obj = new SingleNumber();
        int[] array = {5, 1, 5, 3, 1};
        System.out.println(obj.singleNumber(array));
    }

    //beats 100% users
    public int singleNumber(int[] nums) {
        int ans = 0;
        for (int n : nums) {
            ans ^= n;
        }
        return ans;
    }


    //Beats 38% Users
    public int singleNumber1(int[] nums) {
        Arrays.sort(nums);

        for (int i = 0; i < nums.length - 1; i++) {
            if (nums[i] == nums[i + 1]) {
                i++;
            } else {
                return nums[i];
            }
        }
        return nums[nums.length - 1];
    }
}
