package com.easy.solutions;

public class SearchInsertPosition {
    public static void main(String[] args) {

        SearchInsertPosition obj = new SearchInsertPosition();
        int[] nums = {1, 3, 5, 6};
        int target = 2;
        System.out.println(obj.searchInsert(nums, target));
    }

    //Beats 100.00% Users
    public int searchInsert(int[] nums, int target) {
        int previousData = nums[0];
        int count = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == target) {
                return i;
            }
            if (previousData > target) {
                if (i == 0) {
                    return i;
                }
                return i - 1;
            }
            previousData = nums[i];
        }

        if (nums[nums.length - 1] < target) {
            count = nums.length;
        } else {
            count = nums.length - 1;
        }
        return count;
    }
}
