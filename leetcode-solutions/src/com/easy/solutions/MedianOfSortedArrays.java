package com.easy.solutions;

import javax.print.attribute.standard.Media;
import java.util.Arrays;

public class MedianOfSortedArrays {

    public static void main(String[] args) {
        MedianOfSortedArrays obj = new MedianOfSortedArrays();

//        int[] nums1 = {1,3}, nums2 = {2};

        int[] nums1 = {1,2}, nums2 = {3,4};

        System.out.println(obj.findMedianSortedArrays(nums1, nums2));
    }


    //Beats 35.19% users
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {

        int index = 0;
        int[] arr = new int[nums1.length + nums2.length];

        for(int i = 0; i < nums1.length; i++){
            arr[index] = nums1[i];
            index++;
        }

        for(int j = 0; j < nums2.length; j++){
            arr[index] = nums2[j];
            index++;
        }
        Arrays.sort(arr);

        if(arr.length == 0) {
            return 0;
        } else if(arr.length == 1) {
            return arr[0];
        } else if (arr.length == 2) {
            return (double) (arr[0] + arr[1]) /2;
        }

        int half = arr.length / 2;


        if(half + half != arr.length){
            return arr[half];
        } else {
            return (double) ( arr[half -1] + arr[half])/2;
        }
    }

}
