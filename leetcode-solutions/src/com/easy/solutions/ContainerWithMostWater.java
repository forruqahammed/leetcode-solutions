package com.easy.solutions;

public class ContainerWithMostWater {

    public static void main(String[] args) {
        ContainerWithMostWater obj = new ContainerWithMostWater();
        System.out.println(obj.maxArea(new int[]{1,8,6,2,5,4,8,3,7}));
    }

    public int maxArea(int[] height) {

        int start = 0;
        int end = 0;
        int max = 0;

        for(int i = 0; i < height.length ; i++){
            for(int j = 1; j < height.length; j++){
                if(max < (j-i) * Math.min(height[i], height[j])){
                    max = (j-i) * Math.min(height[i], height[j]);
                }
            }
        }
        return max;
    }
}
