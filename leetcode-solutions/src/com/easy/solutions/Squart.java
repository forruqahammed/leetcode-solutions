package com.easy.solutions;

public class Squart {

    public static void main(String[] args) {
        Squart obj = new Squart();
        System.out.println(obj.mySqrt(68));

    }

    public int mySqrt(int x) {
        if(x <= 1){
            return x;
        }
        int mid = (1 + x)/ 2;
        int p = 1;

        while( p * p <= x){
           p++;
        }
        return p-1;
    }

    // Beats 6.61 % for users
    public int mySqrt1(int x) {

        int division = 1;
        for(int i = 1; i <= x; i++){
            division = x / i;

            if(i == division){
                return division;
            }  else if(division < i) {
                return i - 1;
            }

        }
        return 0;

    }
}
