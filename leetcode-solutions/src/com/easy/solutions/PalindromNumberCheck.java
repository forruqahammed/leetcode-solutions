package com.easy.solutions;

public class PalindromNumberCheck {

    public static void main(String[] args) {
        PalindromNumberCheck palindromNumberCheck = new PalindromNumberCheck();
        System.out.println(palindromNumberCheck.isPalindrome(121));
    }


    // Best Solution for Palindrom nbumber check and beats 100% Users
    public boolean isPalindrome(int x) {

        int leftPortion = 0;
        int sum = 0;
        int previousData = x;

        while (x > 0) {
            leftPortion = x % 10;
            sum = (sum * 10) + leftPortion;
            x = x / 10;
        }

        if (sum == previousData) {
            return true;
        } else {
            return false;
        }
    }

    //Another option for palindrome number check but not fully satisfied and beats a minimum percentage
    public boolean isPalindrome1(int x) {
        String y = String.valueOf(x);
        String reversString = "";
        int length = y.length();

        for (int i = length - 1; i >= 0; i--) {
            reversString += y.charAt(i);
        }

        if (y.equals(reversString)) {
            return true;
        } else {
            return false;
        }
    }

}
