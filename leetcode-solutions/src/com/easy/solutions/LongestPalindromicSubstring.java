package com.easy.solutions;

public class LongestPalindromicSubstring {

    public static void main(String[] args) {
        LongestPalindromicSubstring obj = new LongestPalindromicSubstring();
        System.out.println(obj.longestPalindrome("bababad"));
    }

    public String longestPalindrome(String s) {

  System.out.println(  s.substring(0, 2));
  System.out.println(s);
        char ch[]=s.toCharArray();
        String st = "";
        String rs = "";

        int maxCount = 0;
        int count = 0;
        String longString = "";

        for(int i = 0; i < ch.length; i++){
            st += ch[i];

             rs = "";

            for(int j = st.length() -1; j>= 0; j--){
                rs += st.charAt(j);
            }

            if(st.equals(rs)){
                count++;
            }else {
                count = 0;
                st = "";
                rs = "";
            }

            if(count > maxCount) {
                maxCount = count;
                longString = st;
            }

        }

        return longString;

    }
}
