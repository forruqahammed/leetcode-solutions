package com.easy.solutions;

import java.util.Arrays;

public class MajorityElement {

    public static void main(String[] args) {
        MajorityElement obj = new MajorityElement();

        int nums[]  = {2,2,1,1,1,2,2, 3, 5};

        System.out.println(obj.majorityElement(nums));
    }


    // Beats  38.34%  Users
    public int majorityElement(int[] nums) {
        if (nums.length <= 2) {
            return nums[0];
        }

        int count = 1;
        int max = 1;
        int ans = nums[0];
        Arrays.sort(nums);

        for (int i = 0; i < nums.length - 1; i++) {
            if (nums[i] == nums[i + 1]) {
                count++;
            } else {
                count = 1;
            }

            if (count > max) {
                max = count;
                ans = nums[i];
            }
        }
        return ans;
    }
}
