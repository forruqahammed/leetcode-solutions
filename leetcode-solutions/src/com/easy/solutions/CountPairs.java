package com.easy.solutions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CountPairs {

    public static void main(String[] args) {
        CountPairs obj = new CountPairs();
        List<Integer> nums = Arrays.asList(-6,2,5,-2,-7,-1,3);
        int target = -2;
        System.out.println(obj.countPairs(nums , target ));
    }


    //Beats 98.53% users
    public int countPairs(List<Integer> nums, int target) {

        int count = 0;
        int i =  0;
        while(i<nums.size()){
            int j = i+1;
            while(j < nums.size()  ){
                if(nums.get(i) + nums.get(j ) < target){
                    count++;
                }
                j++;
            }
            i++;
        }

        return count;
    }
}
