package com.hard.solutions;

public class MinimumReplacementToSortArray {

    public static void main(String[] args) {
        MinimumReplacementToSortArray obj = new MinimumReplacementToSortArray();
        int[] nums = {12,9,7,6,17,19,21};
//        int[] nums = {3, 9, 3};
        System.out.println(obj.minimumReplacement(nums));
    }

    public long minimumReplacement(int[] nums) {
        long result = 0;
        if (nums.length < 2) {
            return result;
        }

        for (int i = nums.length - 1; i > 0; i--) {
            if (nums[i] < nums[i - 1]) {
                while (nums[i] < nums[i - 1]) {
                    int div = nums[i] /2;
                    int mod = nums[i] % 2;
                    if(div+ mod >= nums[i]) {
                        nums[i-1] = nums[i-1] - nums[i];
                    }else {
                        nums[i-1] = div + mod;
                    }
                    result++;
                }
            }
        }
        return result;
    }
}
