package com.hard.solutions;

import java.util.Arrays;

public class FirstMissingPositive {

    public static void main(String[] args) {
        FirstMissingPositive obj = new FirstMissingPositive();
        int[] nums = {1, 1};
        System.out.println(obj.firstMissingPositive(nums));
    }

    //Beats 22.65% Users
    public int firstMissingPositive(int[] nums) {
        Arrays.sort(nums);
        boolean isFirst = true;
        int x = 1;

        for (int i = 0; i < nums.length; i++) {
            if (!isFirst && x != nums[i]) {
                x++;
                if (x != nums[i]) {
                    return x;
                }
                if (i == nums.length - 1) {
                    return ++x;
                }
            } else if (!isFirst && i == nums.length - 1) {
                return ++x;
            }
            if (isFirst && nums[i] > 0) {
                if (nums[i] != 1) {
                    return 1;
                } else if (i == nums.length - 1) {
                    return ++x;
                }
                x = nums[i];
                isFirst = false;
            }
        }

        return x;
    }
}
