package com.medium.solutions;

import java.util.ArrayList;
import java.util.List;

public class MinimumNumberOfTaps {

    public static void main(String[] args) {
        MinimumNumberOfTaps obj = new MinimumNumberOfTaps();
        int n = 5;
        int[] ranges = {3, 4, 1, 1, 0, 0};
        System.out.println(obj.minTaps(n, ranges));

    }

    public int minTaps(int n, int[] ranges) {
        int ans = -1;
        for (int i = 0; i < n; i++) {
            int start = i - ranges[i];
            int end = i + ranges[i];
            if (end == n) {
                return i;
            }
        }

        return ans;
    }
}
