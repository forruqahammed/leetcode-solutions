package com.medium.solutions;

public class MinimumPanalty {

    public static void main(String[] args) {
        MinimumPanalty obj = new MinimumPanalty();
        System.out.println(obj.bestClosingTime("YYYYY"));
    }


    // Beats 29.63% of Users
    public int bestClosingTime(String customers) {
        int minPanalty = Integer.MAX_VALUE;
        int[] intArray = new int[customers.length()];
        int result = 0;
        int index = 0;
        int sum = index;
        for (int i = 0; i < customers.length(); i++) {
            intArray[i] = customers.charAt(i);
            sum += addNumber(customers.charAt(i));
        }

        if (sum < minPanalty) {
            System.out.println("sum   = " + sum);
            minPanalty = sum;
            result = index;
        }
        for (int i = 0; i < customers.length(); i++) {

            index++;
            if (customers.charAt(i) == 'Y') {
                sum -= 1;
            } else {
                sum += 1;
            }

            if (sum < minPanalty) {
                System.out.println("sum   = " + sum);
                minPanalty = sum;
                result = index;
                ;
            }
        }

        return result;
    }

    int addNumber(char c) {
        if (c == 'Y') {
            return 1;
        } else {
            return 0;
        }
    }

    //Time limit exceeded
    public int bestClosingTime1(String customers) {
        int minPanalty = Integer.MAX_VALUE;
        int result = 0;

        for (int i = 0; i <= customers.length(); i++) {
            int sum = 0;
            for (int j = 0; j < customers.length(); j++) {
                sum += getCustomerValue(customers.charAt(j), j, i);
            }
            if (minPanalty > sum) {
                minPanalty = sum;
                result = i;
            }
        }
        return result;
    }

    int getCustomerValue(char c, int index, int current) {
        if (c == 'Y' && index >= current) {
            return 1;
        } else if (c == 'N' && index < current) {
            return 1;
        } else {
            return 0;
        }
    }
}
