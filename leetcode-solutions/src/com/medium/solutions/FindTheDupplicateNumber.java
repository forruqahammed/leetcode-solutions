package com.medium.solutions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FindTheDupplicateNumber {
    public static void main(String[] args) {
        LetterCombination obj = new LetterCombination();
        System.out.println(obj.letterCombinations("23"));

    }

    //Solution 1 solved by me and beets 78.60% users
    List<String> letters = new ArrayList<>();
    List<String> stringList = new ArrayList<>();
    public List<String> letterCombinations(String digits) {
        if (digits.isEmpty()) {
            return letters;
        }

        for (int i = 0; i < digits.length(); i++) {
            switch (digits.charAt(i)) {
                case '2': {
                    modifyData("abc");
                    break;
                }
                case '3':
                    modifyData("def");
                    break;
                case '4':
                    modifyData("ghi");
                    break;
                case '5':
                    modifyData("jkl");
                    break;
                case '6':
                    modifyData("mno");
                    break;
                case '7':
                    modifyData("pqrs");
                    break;
                case '8':
                    modifyData("tuv");
                    break;
                case '9':
                    modifyData("wxyz");
                    break;
            }
        }

        return letters;

    }

    public void modifyData(String s) {
        if (letters.isEmpty()) {
            for (int j = 0; j < s.length(); j++) {
                letters.add(Character.toString(s.charAt(j)));
                stringList.add(Character.toString(s.charAt(j)));
            }
        } else {
            letters = new ArrayList<>();
            for (int x = 0; x < stringList.size(); x++) {
                for (int y = 0; y < s.length(); y++) {
                    letters.add(stringList.get(x) + Character.toString(s.charAt(y)));
                }
            }
            stringList = letters;
        }

    }
    //Solution 1 ends


    //Solution 2 get from chatgpt and beats
    public List<String> letterCombinations2(String digits) {
        List<String> result = new ArrayList<>();

        if (digits == null || digits.length() == 0) {
            return result;
        }

        // Mapping of digits to corresponding letters
        Map<Character, String> digitToLetters = new HashMap<>();
        digitToLetters.put('2', "abc");
        digitToLetters.put('3', "def");
        digitToLetters.put('4', "ghi");
        digitToLetters.put('5', "jkl");
        digitToLetters.put('6', "mno");
        digitToLetters.put('7', "pqrs");
        digitToLetters.put('8', "tuv");
        digitToLetters.put('9', "wxyz");

        generateCombinations(digits, "", 0, result, digitToLetters);

        return result;
    }

    private void generateCombinations(String digits, String current, int index, List<String> result, Map<Character, String> digitToLetters) {
        if (index == digits.length()) {
            result.add(current);
            return;
        }

        char digit = digits.charAt(index);
        String letters = digitToLetters.get(digit);

        for (char letter : letters.toCharArray()) {
            generateCombinations(digits, current + letter, index + 1, result, digitToLetters);
        }
    }

}
