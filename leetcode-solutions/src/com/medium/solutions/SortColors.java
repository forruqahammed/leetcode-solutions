package com.medium.solutions;

import java.util.ArrayList;
import java.util.List;

public class SortColors {

    public static void main(String[] args) {
        SortColors obj = new SortColors();
        int[] colors = {2, 0, 2, 1, 1, 0};
        obj.sortColors(colors);
        ;
    }


    //Beats  23.14% users
    public void sortColors(int[] nums) {
        List<Integer> zeroList = new ArrayList<>();
        List<Integer> oneList = new ArrayList<>();
        List<Integer> twoList = new ArrayList<>();

        for (int i = 0; i < nums.length; i++) {
            switch (nums[i]) {
                case 0: {
                    zeroList.add(nums[i]);
                    break;
                }
                case 1: {
                    oneList.add(nums[i]);
                    break;
                }
                case 2: {
                    twoList.add(nums[i]);
                    break;
                }
            }
        }

        int index = 0;
        for (int i = 0; i < zeroList.size(); i++) {
            nums[index] = zeroList.get(i);
            index++;
        }
        for (int i = 0; i < oneList.size(); i++) {
            nums[index] = oneList.get(i);
            index++;
        }

        for (int i = 0; i < twoList.size(); i++) {
            nums[index] = twoList.get(i);
            index++;
        }
        for (int n : nums) {
            System.out.print(n + " ");
        }
    }
}
